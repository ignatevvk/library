package ru.sber.spring.java13springmy.libraryproject.service;

import ru.sber.spring.java13springmy.libraryproject.dto.AuthorDTO;
import ru.sber.spring.java13springmy.libraryproject.mapper.AuthorMapper;
import ru.sber.spring.java13springmy.libraryproject.model.Author;
import ru.sber.spring.java13springmy.libraryproject.repository.AuthorRepository;
import org.springframework.stereotype.Service;

@Service
public class AuthorService
      extends GenericService<Author, AuthorDTO> {
    protected AuthorService(AuthorRepository authorRepository,
                            AuthorMapper authorMapper) {
        super(authorRepository, authorMapper);
    }
}
