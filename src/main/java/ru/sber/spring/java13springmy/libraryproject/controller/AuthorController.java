package ru.sber.spring.java13springmy.libraryproject.controller;

import ru.sber.spring.java13springmy.libraryproject.dto.AuthorDTO;
import ru.sber.spring.java13springmy.libraryproject.model.Author;
import ru.sber.spring.java13springmy.libraryproject.service.AuthorService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/authors")
@Tag(name = "Авторы",
     description = "Контроллер для работы с авторами книг библиотеки")
public class AuthorController
      extends GenericController<Author, AuthorDTO> {
    
    private AuthorService authorService;
    public AuthorController(AuthorService authorService) {
        super(authorService);
        this.authorService = authorService;
    }
    
//    @Operation(description = "Добавить книгу к автору", method = "addBook")
//    @RequestMapping(value = "/addBook", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Author> addAuthor(@RequestParam(value = "bookId") Long bookId,
//                                            @RequestParam(value = "authorId") Long authorId) {
//        Book book = bookRepository.findById(bookId).orElseThrow(() -> new NotFoundException("Книга с переданным ID не найдена"));
//        Author author = genericRepository.findById(authorId).orElseThrow(() -> new NotFoundException("Автор с таким ID не найден"));
//        author.getBooks().add(book);
//        return ResponseEntity.status(HttpStatus.OK).body(genericRepository.save(author));
//    }
}
