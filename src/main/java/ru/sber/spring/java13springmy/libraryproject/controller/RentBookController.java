package ru.sber.spring.java13springmy.libraryproject.controller;

import ru.sber.spring.java13springmy.libraryproject.dto.BookRentInfoDTO;
import ru.sber.spring.java13springmy.libraryproject.model.BookRentInfo;
import ru.sber.spring.java13springmy.libraryproject.service.BookRentInfoService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rent/info")
@Tag(name = "Аренда книг",
     description = "Контроллер для работы с арендой/выдачей книг пользователям библиотеки")
public class RentBookController
      extends GenericController<BookRentInfo, BookRentInfoDTO> {
    public RentBookController(BookRentInfoService bookRentInfoService) {
        super(bookRentInfoService);
    }
}
