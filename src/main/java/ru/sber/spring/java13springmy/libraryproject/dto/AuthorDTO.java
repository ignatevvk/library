package ru.sber.spring.java13springmy.libraryproject.dto;

import lombok.*;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class AuthorDTO
      extends GenericDTO {
    private String fio;
    private LocalDate birthDate;
    private String description;
    private Set<Long> booksIds;
}
