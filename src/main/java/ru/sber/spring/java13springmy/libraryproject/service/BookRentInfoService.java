package ru.sber.spring.java13springmy.libraryproject.service;

import ru.sber.spring.java13springmy.libraryproject.dto.BookRentInfoDTO;
import ru.sber.spring.java13springmy.libraryproject.mapper.BookRentInfoMapper;
import ru.sber.spring.java13springmy.libraryproject.model.BookRentInfo;
import ru.sber.spring.java13springmy.libraryproject.repository.BookRentInfoRepository;
import org.springframework.stereotype.Service;

@Service
public class BookRentInfoService
      extends GenericService<BookRentInfo, BookRentInfoDTO> {
    private BookRentInfoRepository bookRentInfoRepository;
    
    protected BookRentInfoService(BookRentInfoRepository bookRentInfoRepository,
                                  BookRentInfoMapper bookRentInfoMapper) {
        super(bookRentInfoRepository, bookRentInfoMapper);
        this.bookRentInfoRepository = bookRentInfoRepository;
    }
}
