package ru.sber.spring.java13springmy.libraryproject.service;

import ru.sber.spring.java13springmy.libraryproject.dto.UserDTO;
import ru.sber.spring.java13springmy.libraryproject.mapper.UserMapper;
import ru.sber.spring.java13springmy.libraryproject.model.User;
import ru.sber.spring.java13springmy.libraryproject.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService
      extends GenericService<User, UserDTO> {
    private UserRepository userRepository;
    
    protected UserService(UserRepository userRepository,
                          UserMapper userMapper) {
        super(userRepository, userMapper);
        this.userRepository = userRepository;
    }
}
