package ru.sber.spring.java13springmy.libraryproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.spring.java13springmy.libraryproject.model.Author;

@Repository
public interface AuthorRepository
      extends GenericRepository<Author> {
}
