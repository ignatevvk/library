package ru.sber.spring.java13springmy.libraryproject.repository;

import org.springframework.stereotype.Repository;
import ru.sber.spring.java13springmy.libraryproject.model.BookRentInfo;

@Repository
public interface BookRentInfoRepository
      extends GenericRepository<BookRentInfo> {
}
