package ru.sber.spring.java13springmy.libraryproject.controller;

import ru.sber.spring.java13springmy.libraryproject.dto.UserDTO;
import ru.sber.spring.java13springmy.libraryproject.model.User;
import ru.sber.spring.java13springmy.libraryproject.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи",
     description = "Контроллер для работы с пользователями библиотеки")
public class UserController
      extends GenericController<User, UserDTO> {
    
    public UserController(UserService userService) {
        super(userService);
    }
}
