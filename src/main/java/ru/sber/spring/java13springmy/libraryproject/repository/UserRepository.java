package ru.sber.spring.java13springmy.libraryproject.repository;

import org.springframework.stereotype.Repository;
import ru.sber.spring.java13springmy.libraryproject.model.User;

@Repository
public interface UserRepository
      extends GenericRepository<User> {
}
